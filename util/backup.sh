#!/bin/bash
echo "Server backup in progress..." | wall -n
cd /srv/minecraft/
git add --all
git commit -m "Auto-commit from misato: $(date) "
git push -u origin master
echo "Server backup complete. -misato ^__^ " | wall -n

