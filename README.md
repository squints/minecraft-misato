# README #

This is an archive / codebase for the Minecraft server(s) on Misato.

### Features ###

-Forge & Vanilla
-Startup scripts
-Ubuntu Upstart scripts
-Automatic text messages for server statuses
-Automatic backups every 2 hours

### TODO ###

+Text alerts on new players joining
+Text alerts on server restarts / switching between MC versions
+Minor tweaks and bugfixes